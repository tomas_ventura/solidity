import "@typechain/hardhat";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-waffle";
import "solidity-coverage";
import "hardhat-gas-reporter";
import { HardhatUserConfig } from "hardhat/config";


const config: HardhatUserConfig = {
  defaultNetwork: "hardhat",
  solidity: {
    compilers: [{ version: "0.7.3", settings: {} }],
  },
  networks: {
    hardhat: {},
    localhost: {},
    coverage: {
      url: "http://127.0.0.1:8555", // Coverage launches its own ganache-cli client
    },
    rinkeby: {
      url: 'https://eth-rinkeby.alchemyapi.io/v2/0EAPw5bSg16YUGmIcylQsk-0SYJh3Ojh',
      accounts: [`0x${'0536d72e96d2b684a215f4f6ffa9e451bdcfd211c218f743a6dacbe6a50b2ec6'}`],
      gasPrice: 8000000000
    }
  }

};

export default config
